#ifndef PNGPATHGENERATOR_H
#define PNGPATHGENERATOR_H

#include "pathgenerator.h"


class PngPathGenerator : PathGenerator
{
private:
    PngPathGenerator();

    QString getFileExtension() override;

    friend class Screenshot;
};

#endif // PNGPATHGENERATOR_H
