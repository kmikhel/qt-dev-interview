#include <QDateTime>
#include <stdexcept>

#include "pathgenerator.h"


PathGenerator::PathGenerator()
{
}


QString PathGenerator::generateFromDateTime()
{
    QDateTime currDateTime = QDateTime::currentDateTime();

    QString folderName = currDateTime.toString("dd-MM-yyyy");
    QString fileName = currDateTime.toString("hh-mm-ss");

    QString fileExtensionAsStr = getFileExtension();

    return QString("%1/%2.%3").arg(folderName, fileName, fileExtensionAsStr);
}

