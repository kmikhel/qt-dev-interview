#ifndef PATHGENERATOR_H
#define PATHGENERATOR_H

#include <QString>


class PathGenerator
{
protected:
    PathGenerator();
    QString generateFromDateTime();
    virtual QString getFileExtension() = 0;
};


#endif // PATHGENERATOR_H
