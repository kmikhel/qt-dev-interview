#include <QCoreApplication>

#include "screenshot.h"
#include "windowcapture.h"


int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    Screenshot().save();

    WindowCapture().save();

    return app.exec();
}
