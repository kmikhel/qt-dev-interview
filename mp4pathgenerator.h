#ifndef MP4PATHGENERATOR_H
#define MP4PATHGENERATOR_H


#include "pathgenerator.h"


class Mp4PathGenerator : PathGenerator
{
private:
    Mp4PathGenerator();

    QString getFileExtension() override;

    friend class WindowCapture;
};

#endif // MP4PATHGENERATOR_H
